package com.afpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Categorie {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CATEGORIE_SEQ")
    private int id;
	
	@NotBlank
	private String label;
	

}
