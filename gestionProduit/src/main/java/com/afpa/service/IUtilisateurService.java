package com.afpa.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.afpa.dto.CategorieDto;
import com.afpa.dto.UtilisateurDto;
import com.afpa.entity.Utilisateur;

public interface IUtilisateurService {
	
	public List<UtilisateurDto>all();

	public List<UtilisateurDto> chercherToutLeMonde(int page);

	public String deleteById(int id);

	public Optional<UtilisateurDto> findById(int id);

	public Boolean ajouterUtilisateur(String login, String password, Boolean admin);

	public Boolean mettreAJourUtilisateur(UtilisateurDto utilisateurDto);

	public List<UtilisateurDto> chercherToutesLesUtilisateurParLogin(int page);

	public List<UtilisateurDto> chercherToutesLesUtilisateurParLoginDesc(int page);

	public List<UtilisateurDto> chercherToutesLesUtilisateurParIdDesc(int page);

	public List<UtilisateurDto> chercherToutesLesUtilisateurParId(int page);

}