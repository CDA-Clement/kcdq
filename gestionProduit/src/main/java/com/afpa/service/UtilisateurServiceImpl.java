package com.afpa.service;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.dao.UtilisateurRepository;
import com.afpa.dto.CategorieDto;
import com.afpa.dto.UtilisateurDto;
import com.afpa.entity.Utilisateur;

@Service
public class UtilisateurServiceImpl implements IUtilisateurService{
	static int INC = 5;
	public static int fin;
	@Autowired
	private UtilisateurRepository userRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	public List<UtilisateurDto> chercherToutLeMonde(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<UtilisateurDto> maliste =this.userRepository
				.findAll(firstPageWithTwoElements)
				.stream()
				.map(u->UtilisateurDto.builder()
						.id(u.getId())
						.login(u.getLogin())
						.password(u.getPassword())
						.admin(u.getAdmin())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}

	@Override
	public String deleteById(int id) {
		if (this.userRepository.existsById(id)) {
			try {
				this.userRepository.deleteById(id);	
				return "OK";
			} catch (Exception e) {
				return "KO_SQL_EXCEPTION";				
			}
		}
		return "KO";
	}

	@Override
	public Optional<UtilisateurDto> findById(int id) {
		Optional<Utilisateur> user = this.userRepository.findById(id);
		Optional<UtilisateurDto> res = Optional.empty();
		if(user.isPresent()) {
			Utilisateur u = user.get();
			UtilisateurDto userDto = this.modelMapper.map(u, UtilisateurDto.class);
			res = Optional.of(userDto);
		}
		return res;
	}

	@Override
	public Boolean ajouterUtilisateur(String login, String password, Boolean admin) {
		Boolean creer = true;
		Iterable<Utilisateur> utilisateurs = this.userRepository.findAll();
		for (Utilisateur utilisateur : utilisateurs) {
			if (utilisateur.getLogin().equals(login)) {
				creer = false;
				break;
			}
		}
		if (creer) {
			Utilisateur utilisateur1 = Utilisateur.builder().login(login).password(password).admin(admin).build();
			try {
				userRepository.save(utilisateur1);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		return creer;
	}

	@Override
	public Boolean mettreAJourUtilisateur(UtilisateurDto utilisateurDto) {
		Utilisateur utilisateur = Utilisateur.builder()
				.id(utilisateurDto.getId())
				.login(utilisateurDto.getLogin())
				.password(utilisateurDto.getPassword())
				.admin(utilisateurDto.getAdmin())
				.build();
		try {
			this.userRepository.save(utilisateur);			
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public List<UtilisateurDto> chercherToutesLesUtilisateurParLogin(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<UtilisateurDto> maliste = this.userRepository.findAllByOrderByLoginAsc(firstPageWithTwoElements)
				.stream().map(e -> UtilisateurDto.builder().id(e.getId()).login(e.getLogin()).build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}

	@Override
	public List<UtilisateurDto> chercherToutesLesUtilisateurParLoginDesc(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<UtilisateurDto> maliste = this.userRepository.findAllByOrderByLoginDesc(firstPageWithTwoElements)
				.stream().map(e -> UtilisateurDto.builder().id(e.getId()).login(e.getLogin()).build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}

	@Override
	public List<UtilisateurDto> chercherToutesLesUtilisateurParIdDesc(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<UtilisateurDto> maliste = this.userRepository.findAllByOrderByIdDesc(firstPageWithTwoElements).stream()
				.map(e -> UtilisateurDto.builder().id(e.getId()).login(e.getLogin()).build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}

	@Override
	public List<UtilisateurDto> chercherToutesLesUtilisateurParId(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<UtilisateurDto> maliste = this.userRepository.findAllByOrderByIdAsc(firstPageWithTwoElements).stream()
				.map(e -> UtilisateurDto.builder().id(e.getId()).login(e.getLogin()).build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}

	@Override
	public List<UtilisateurDto> all() {
		List<UtilisateurDto> maliste =this.userRepository
				.findAll()
				.stream()
				.map(u->UtilisateurDto.builder()
						.id(u.getId())
						.login(u.getLogin())
						.password(u.getPassword())
						.admin(u.getAdmin())
						.build())
				.collect(Collectors.toList());
		System.err.println("maliste "+maliste.size());
		return maliste;
	}





}
