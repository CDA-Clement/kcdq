package com.afpa.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.dto.CategorieDto;
import com.afpa.dto.ProduitDto;
import com.afpa.dto.ReponseStatut;
import com.afpa.dto.UtilisateurDto;
import com.afpa.service.CategorieServiceImpl;
import com.afpa.service.ICategorieService;
import com.afpa.service.IProduitService;
import com.afpa.service.IUtilisateurService;
import com.afpa.service.UtilisateurServiceImpl;



@Controller
public class UtilisateurController {

	@Autowired
	private IProduitService produitService;

	@Autowired
	private ICategorieService categorieService;

	@Autowired
	private IUtilisateurService userService;

	static String origin = "";

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value = "origin", defaultValue = "accueil") String param1,
			@RequestParam(value = "page", defaultValue = "0") String page,
			@RequestParam(value = "trier", defaultValue = "null") String trier, ModelAndView mv,
			HttpServletRequest request) {
		System.err.println("par ici login en get +param1 " + param1);

		mv.setViewName("login");
		HttpSession session = request.getSession();
		UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
		origin = param1;
		if (utilisateur != null) {
			mv.setViewName("accueil");
		} else {
			mv.setViewName("login");
		}
		mv.addObject("page", page);
		mv.addObject("trier", trier);
		return mv;
	}

	@RequestMapping(value="/login1", method = RequestMethod.POST,params = {"username","password","page", "trier"})
	public  ModelAndView loginUser(
			@RequestParam(value = "username") String login,
			@RequestParam(value = "password") String password,
			@RequestParam(value = "page") String pageStr,
			@RequestParam(value = "trier") String trier,
			ModelAndView mv, HttpServletRequest request) {

		int page = 0;

		System.out.println("pageStr: "+pageStr);
		if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {
			pageStr = "0";
		}else {
			page = Integer.parseInt(pageStr);
			mv.addObject("page", page);

		}
		mv.addObject("trier", trier);

		System.out.println("origin"+origin);

		System.err.println("par la login en post");
		if(origin.contentEquals("listCategorie")) {
			List<CategorieDto> liste2=this.categorieService.chercherToutesLesCategories(0);
			mv.addObject("liste", liste2);
		}

		if(origin.contentEquals("listProduits")) {
			List<ProduitDto> liste2=this.produitService.chercherToutesLesProduits(0);
			mv.addObject("liste", liste2);
		}

		if(origin.contains("showproduit")) {
			String[]tabOrigin=origin.split("\\d+");
			String origin1 = tabOrigin[0];
			tabOrigin=origin.split("[a-zA-Z]+");
			origin=origin1;
			Optional<ProduitDto> pd = produitService.findById(Integer.parseInt(tabOrigin[1]));
			if(pd.isPresent()) {
				mv.addObject("produit", pd.get());				
			}
		}

		if(origin.contains("showCategorie")) {
			String[]tabOrigin=origin.split("\\d+");
			String origin1 = tabOrigin[0];
			tabOrigin=origin.split("[a-zA-Z]+");
			origin=origin1;
			Optional<CategorieDto> cd = categorieService.findById(Integer.parseInt(tabOrigin[1]));
			if(cd.isPresent()) {
				mv.addObject("categorie", cd.get());				

			}
		}
System.err.println("recupelogin "+login);
System.err.println("recupepassaword "+password);
		List<UtilisateurDto> liste = userService.all();
		System.err.println("taille "+liste.size());

		for (UtilisateurDto utilisateurDto : liste) {
			System.err.println("username "+utilisateurDto.getLogin()+"password "+utilisateurDto.getPassword());
			if (utilisateurDto.getLogin().equals(login) && utilisateurDto.getPassword().equals(password)) {
				System.out.println("trouvé:" + utilisateurDto.toString());
				Optional<UtilisateurDto> utilisateurOptional = this.userService.findById(utilisateurDto.getId());
				if(utilisateurOptional.isPresent()) {
					HttpSession session = request.getSession();
					session.setAttribute("utilisateur", UtilisateurDto.builder()
							.login(login)
							.admin(utilisateurOptional.get().getAdmin())
							.build());


					mv.addObject("message", "authentification reussi");
					if(origin.equals("login")) {
						origin="accueil";
					}

					mv.setViewName(origin);
					return mv;
				}
			} else {
				mv.addObject("message", "utilisateur n'existe pas");
			}
		}
		mv.setViewName(origin);


		return mv;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(@RequestParam(value = "origin") String param1, ModelAndView mv,
			HttpServletRequest request) {
		System.err.println("par ici logout en get");
		HttpSession session = request.getSession();
		session.invalidate();
		origin = param1;

		mv.setViewName("accueil");
		return mv;
	}



	@GetMapping(value = "/showUtilisateur", params = { "id" })
	public ModelAndView showUtilisateur(@RequestParam(value = "id") int id,
			@RequestParam(value = "page") String pageStr,
			@RequestParam(value = "trier") String trier,
			ModelAndView mv, HttpServletRequest request) {

		HttpSession session = request.getSession();
		int page = 0;

		if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {
			pageStr = "0";
		}else {
			page = Integer.parseInt(pageStr);
		}
		UtilisateurDto utilisateur1 = (UtilisateurDto) session.getAttribute("utilisateur");

		if (utilisateur1.getAdmin() != null) {

			mv.setViewName("showUtilisateur");
			Optional<UtilisateurDto> userOptional = this.userService.findById(id);
			if (userOptional.isPresent()) {
				UtilisateurDto utilisateur = new UtilisateurDto();
				utilisateur = userOptional.get();
				System.err.println("user" + utilisateur.toString());
				mv.addObject("utilisateur", utilisateur);
				mv.addObject("page", pageStr);
				mv.addObject("trier", trier);
			} else {
				mv.addObject("message", "l'utilisateur n'existe pas");
			}
		} else {
			mv.addObject("message", "vous n'avez pas les droits super Admin");
			mv.setViewName("accueil");
		}

		return mv;
	}

	@PostMapping(value = "/supprimerUtilisateur", params = { "id" })
	public ModelAndView supprimerUtilisateur(@RequestParam(value = "id") Integer id, ModelAndView mv, HttpServletRequest request) {
		HttpSession session = request.getSession();
		UtilisateurDto utilisateur1 = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur1.getAdmin() != null) {

			mv.setViewName("listeUtilisateur");
			String idParam = id.toString();
			String msg = "";
			ReponseStatut statut = ReponseStatut.OK;

			if (idParam == null || idParam.length() == 0) {
				statut = ReponseStatut.KO;
				msg = "le parametre id est obligatoire";
				mv.addObject("message", msg);
			} else {
				if (!idParam.matches("^\\p{Digit}+$")) {
					statut = ReponseStatut.KO;
					msg = "le parametre id doit etre un numero";
					mv.addObject("message", msg);
				} else {
					id = Integer.parseInt(idParam);
					String rep = this.userService.deleteById(id);
					switch (rep) {
					case "OK":
						statut = ReponseStatut.OK;
						System.err.println("supprimé");
						msg = "suppression reussie avec succes";
						mv.addObject("message", msg);
						break;
					case "KO_SQL_EXCEPTION":
						System.err.println("PAS supprimé sqlexception");
						statut = ReponseStatut.KO;
						msg = "aie";
						System.out.println(msg);
						mv.addObject("message", msg);
						return mv;
					case "KO":
						System.err.println("PAS supprimé id");
						statut = ReponseStatut.KO;
						msg = "aucun utilisateur n'a cet id " + id;
						mv.addObject("message", msg);
						break;
					default:
						System.err.println("PAS supprimé je sais pas");
						statut = ReponseStatut.KO;
						msg = "erreur inconnu, contacter le support";
						mv.addObject("message", msg);
						break;
					}
				}
			}
		} else {
			mv.addObject("message", "vous n'avez pas les droits super Admin");
			mv.setViewName("accueil");
		}
		return mv;
	}

	@GetMapping(value = "/ajouterUtilisateur",params = {"page", "trier" })
	public ModelAndView formulaireUtilisateur(
			@RequestParam(value = "page") String page,
			@RequestParam(value = "trier") String trier,
			ModelAndView mv, HttpServletRequest request) {
		HttpSession session = request.getSession();
		UtilisateurDto utilisateur1 = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur1.getAdmin() != null) {
			mv.addObject("page", page);
			mv.addObject("trier", trier);
			mv.setViewName("ajouterUtilisateur");
		} else {
			mv.addObject("message", "vous n'avez pas les droits super Admin");
			mv.setViewName("accueil");
		}

		return mv;

	}

	@PostMapping(value = "/ajouterUtilisateur", params = { "login", "password", "page", "trier" })
	public ModelAndView ajouterUtilisateur(
			@RequestParam(value = "login") String login,
			@RequestParam(value = "password") String password,
			@RequestParam(value = "page") String page,
			@RequestParam(value = "trier") String trier,
			@RequestParam(value = "admin", defaultValue = "off") String adminStr,
			ModelAndView mv, HttpServletRequest request) {
		Boolean admin=false;

		if(adminStr.equals("on")) {
			admin=true;
		}
		System.err.println("admin: "+admin);
		HttpSession session = request.getSession();
		UtilisateurDto utilisateur1 = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur1.getAdmin() != null) {

			boolean test = this.userService.ajouterUtilisateur(login, password,admin);
			if (test) {
				mv.addObject("message", "l'utilisateur a bien été ajouté");
			} else {
				mv.addObject("message", "l'utilisateur n'as pas été rajouté");
			}
			mv.setViewName("ajouterUtilisateur");
		} else {
			mv.addObject("message", "vous n'avez pas les droits super Admin");
			mv.setViewName("accueil");
		}
		listUtilisateur(page, trier, mv, request);
		return mv;

	}

	@RequestMapping(value = "/editUtilisateur", method = RequestMethod.GET, params = { "id","page","trier" })

	public ModelAndView formulaireEditUtilisateur(
			@RequestParam(value = "id") int id,
			@RequestParam(value = "page") String page,
			@RequestParam(value = "trier") String trier,
			ModelAndView mv, HttpServletRequest request) {
		System.out.println("Utilisateurlala : " + id);

		HttpSession session = request.getSession();
		UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur == null) {
			mv.setViewName("login");
		}else {

			mv.setViewName("editUtilisateur");
			Optional<UtilisateurDto> utilisateurOptional = this.userService.findById(id);
			if (utilisateurOptional.isPresent()) {
				mv.addObject("utilisateuraedit", utilisateurOptional.get().getId());
			} else {
				mv.addObject("message", "l'utilisateur n'existe pas");
			}

		}
		mv.addObject("page", page);
		mv.addObject("trier", trier);
		return mv;
	}

	@RequestMapping(value = "/editUtilisateur", method = RequestMethod.POST)
	public ModelAndView editCategorie(
			@RequestParam(value = "login") String login,
			@RequestParam(value = "password") String password,
			@RequestParam(value = "utilisateuraedit") int id ,
			@RequestParam(value = "admin", defaultValue = "off") String adminStr ,
			@RequestParam(value = "page") String page ,
			@RequestParam(value = "trier", defaultValue = "null") String trier ,
			ModelAndView mv, HttpServletRequest request) {
		Boolean admin=false;
		System.out.println("Utilisateurlalapost : " + login);
		if(adminStr.equals("on")) {
			admin=true;
		}

		HttpSession session = request.getSession();
		UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur == null) {
			mv.setViewName("login");
		}else {
			Optional<UtilisateurDto> userOpDto = this.userService.findById(id);
			if (userOpDto.isPresent()) {
				userOpDto.get().setLogin(login);
				userOpDto.get().setPassword(password);
				userOpDto.get().setAdmin(admin);
				this.userService.mettreAJourUtilisateur(userOpDto.get());
				System.err.println("utilisateur" + userOpDto.get().toString());
				mv.addObject("utilisateur", userOpDto.get());
				mv.addObject("page", page);
				mv.addObject("trier", trier);
				listUtilisateur(page, trier, mv, request);
				
			} else {

				mv.addObject("message", "l'utilisateur n'existe pas");
			}
		}
		return mv;
	}





	@RequestMapping(value = "/listUtilisateur", method = RequestMethod.GET, params = {"page", "trier"})
	public ModelAndView listUtilisateur(
			@RequestParam(value = "page", defaultValue = "0") String pageStr,
			@RequestParam(value = "trier", defaultValue = "null") String trier,
			ModelAndView mv, HttpServletRequest request) {
		int page = 0;
		mv.setViewName("listUtilisateur");
		System.out.println("pageStr: "+pageStr);
		if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {
			pageStr = "0";
		}else {
			page = Integer.parseInt(pageStr);
			mv.addObject("page", page);
			mv.addObject("page1", pageStr);

		}
		mv.addObject("trier", trier);

		if(trier.equals("loginUp")) {
			List<UtilisateurDto> liste = this.userService.chercherToutesLesUtilisateurParLogin(page);
			mv.addObject("fin", UtilisateurServiceImpl.fin );
			mv.addObject("liste", liste);
		}

		if(trier.equals("loginDown")) {
			List<UtilisateurDto> liste = this.userService.chercherToutesLesUtilisateurParLoginDesc(page);
			mv.addObject("fin", UtilisateurServiceImpl.fin );
			mv.addObject("liste", liste);
		}


		if(trier.equals("idDown")) {
			List<UtilisateurDto> liste = this.userService.chercherToutesLesUtilisateurParIdDesc(page);
			mv.addObject("fin", UtilisateurServiceImpl.fin );
			mv.addObject("liste", liste);
		}

		if(trier.equals("idUp")) {
			List<UtilisateurDto> liste = this.userService.chercherToutesLesUtilisateurParId(page);
			mv.addObject("fin", UtilisateurServiceImpl.fin );
			mv.addObject("liste", liste);
		}
		if(trier.equals("idUp")) {

		}
		if(trier.equals("null")) {

			List<UtilisateurDto> liste = this.userService.chercherToutLeMonde(page);
			for (UtilisateurDto UtilisateurDto : liste) {
				System.err.println(UtilisateurDto.toString());
				mv.addObject("fin", UtilisateurServiceImpl.fin );
				mv.addObject("liste", liste);
			}
		}


		return mv;

	}

}
