package com.afpa.security;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.afpa.dao.UtilisateurRepository;
import com.afpa.entity.Utilisateur;

@Service
public class UtilisateurDetailsService implements UserDetailsService {
 
    @Autowired
    private final UtilisateurRepository utilisateurRepository;
    
    public UtilisateurDetailsService(UtilisateurRepository utilisateurRepository) {
        this.utilisateurRepository = utilisateurRepository;
    }
 
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Objects.requireNonNull(username);
        Utilisateur user = utilisateurRepository.findUserByLogin(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
       
        return new UtilisateurPrincipal(user);
    }
}
