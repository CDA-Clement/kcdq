package com.afpa.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Utilisateur;

@Repository
public interface UtilisateurRepository extends PagingAndSortingRepository<Utilisateur, Integer> {

	public List<Utilisateur> findAll();
	
	public Page<Utilisateur> findAll(Pageable firstPageWithTwoElements);

	public Page<Utilisateur> findAllByOrderByLoginAsc(Pageable pageable);

	public Page<Utilisateur> findAllByOrderByIdAsc(Pageable pageable);

	public Page<Utilisateur> findAllByOrderByLoginDesc(Pageable pageable);

	public Page<Utilisateur> findAllByOrderByIdDesc(Pageable pageable);

	Optional<Utilisateur> findUserByLogin(String login);

}
