<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<style>
figure {
	border: thin #343a40 solid;
	background-color: #343a40;
	display: flex;
	flex-flow: column;
	padding: 5px;
	margin: auto;
}

figcaption {
	background-color: #343a40;
	color: #fff;
	font: italic smaller sans-serif;
	padding: 3px;
	text-align: center;
}
</style>
<meta charset="UTF-8">
<title>Accueil</title>
<link rel="stylesheet" type="text/css"
	href="resources/bootstrap/css/bootstrap.min.css">
<link href="resources/fontawesome/css/all.min.css" rel="stylesheet">


</head>
<body>
	<jsp:include page="header.jsp">
		<jsp:param name="origin" value="accueil" />
	</jsp:include>

	<c:if test="${not empty message }">
		<div class="alert alert-warning" role="alert">
			<p align="center" class="parap">${message}</p>
		</div>
	</c:if>

	<div class="container">
		<div class="row center-block"
			style="text-align: center; display: block; margin: auto;">
			<!-- Portfolio Section -->
			<section class="page-section portfolio" id="portfolio">
				<div class="container">

					<!-- Portfolio Section Heading -->
					<h2
						class="page-section-heading text-center text-uppercase text-secondary mb-0">Accueil</h2>

					<!-- Icon Divider -->
					<div class="divider-custom">
						<div class="divider-custom-line"></div>
						<div class="divider-custom-icon">
							<i class="fas fa-star"></i> <i class="fas fa-star"></i> <i
								class="fas fa-star"></i> <i class="fas fa-star"></i> <i
								class="fas fa-star"></i>
						</div>
						<div class="divider-custom-line"></div>
					</div>

					<!-- Portfolio Grid Items -->
					<div class="row d-flex justify-content-center">

						<!-- Portfolio Item 1 -->
						<div class="col-md-6 col-lg-4">
							<div class="portfolio-item mx-auto" data-toggle="modal"
								data-target="#portfolioModal1">
								<div
									class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
									<div
										class="portfolio-item-caption-content text-center text-white">
										<i class="fas fa-plus fa-3x"></i>
									</div>
								</div>
								<a href="listProduit?page=0&trier=null"><figure>
										<img class="img-fluid" src="resources/image/cake.png"
											alt="Produit">
										<figcaption>Produits</figcaption>
									</figure></a>
							</div>
						</div>

						<!-- Portfolio Item 2 -->
						<div class="col-md-6 col-lg-4">
							<div class="portfolio-item mx-auto" data-toggle="modal"
								data-target="#portfolioModal2">
								<div
									class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
									<div
										class="portfolio-item-caption-content text-center text-white">
										<i class="fas fa-plus fa-3x"></i>
									</div>
								</div>
								<a href="listCategorie?page=0&trier=null">
									<figure>
										<img class="img-fluid" src="resources/image/cabin.png"
											alt="Categorie">
										<figcaption>Catégories</figcaption>
									</figure>
								</a>
							</div>
						</div>
						<c:if test="${not empty utilisateur&&utilisateur.admin==true }">
							<!-- Portfolio Item 3 -->
							<div class="col-md-6 col-lg-4">
								<div class="portfolio-item mx-auto" data-toggle="modal"
									data-target="#portfolioModal3">
									<div
										class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
										<div
											class="portfolio-item-caption-content text-center text-white">
											<i class="fas fa-plus fa-3x"></i>
										</div>
									</div>
									<a href="listUtilisateur?page=0&trier=null">
										<figure>
											<img class="img-fluid" src="resources/image/safe.png" alt="">
											<figcaption>Utilisateurs</figcaption>
										</figure>
									</a>
								</div>
							</div>
					</div>
					</c:if>
					<!-- /.row -->

				</div>
			</section>
		</div>
	</div>

	<script src="resources/jquery/jquery-3.3.1.slim.min.js"></script>
	<script src="resources/bootstrap/js/bootstrap.bundle.min.js"></script>
	<%@include file="footer.jsp"%>
</body>
</html>