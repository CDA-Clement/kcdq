<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<footer
	class="page-footer font-small special-color-dark pt-4 fixed-bottom bg-dark">
	<div class="container">
		<ul class="list-unstyled list-inline text-center">
			<li class="list-inline-item"><a class="btn-floating btn-fb mx-1">
					<i class="fab fa-facebook-f" style="color: #fff;"> </i>
			</a></li>
			<li class="list-inline-item"><a class="btn-floating btn-tw mx-1">
					<i class="fab fa-twitter" style="color: #fff;"> </i>
			</a></li>
			<li class="list-inline-item"><a
				class="btn-floating btn-gplus mx-1"> <i
					class="fab fa-google-plus-g" style="color: #fff;"> </i>
			</a></li>
			<li class="list-inline-item"><a class="btn-floating btn-li mx-1">
					<i class="fab fa-linkedin-in" style="color: #fff;"> </i>
			</a></li>
			<li class="list-inline-item"><a
				class="btn-floating btn-dribbble mx-1"> <i
					class="fab fa-dribbble" style="color: #fff;"> </i>
			</a></li>
		</ul>

		<div class="footer-copyright text-center py-3">
			<p style="color: #fff;">
				© 2019 Copyright: <a
					href="https://www.afpa.fr/centre/centre-de-roubaix"
					style="color: #fff;"> Team from ilot 2</a>
			</p>
			<c:if test="${empty utilisateur }">
				<p style="color: #fff;">Visiteur</p>
			</c:if>
			<c:if
				test="${not empty utilisateur&&utilisateur.admin==null||not empty utilisateur&&utilisateur.admin==false}">
				<p style="color: #fff;">Vous êtes connecté en tant qu'utilisateur</p>
			</c:if>
			<c:if test="${not empty utilisateur&&utilisateur.admin==true}">
				<p style="color: #fff;">Vous êtes connecté en tant que SuperUser</p>
			</c:if>
		</div>
	</div>
</footer>