<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="resources/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="resources/jquery/clickable.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/css.css">
<link rel="stylesheet" type="text/css"
	href="resources/bootstrap/css/bootstrap.min.css">
<link href="resources/fontawesome/css/all.min.css" rel="stylesheet">
<title>Aperçu des catégories</title>
</head>
<body>
	<jsp:include page="header.jsp" >
  <jsp:param name="origin" value="showCategorie${categorie.id}" />
</jsp:include>
	<c:if test="${not empty categorie }">
		<div class="container my-4">
			<div class="row justify-content-md-center">
				<div class="col card col-lg-8\">
					<form>
						<div class="form-group row">
							<label for="idPers" class="col-sm-2 col-form-label">Identifiant</label>
							<div class="col-sm-10">
								<input type="text" readonly class="form-control-plaintext"
									id="idPers" value="${categorie.id}">
							</div>
						</div>
						<div class="form-group row">
							<label for="idPers" class="col-sm-2 col-form-label">Nom</label>
							<div class="col-sm-10">
								<input type="text" readonly class="form-control-plaintext"
									id="idPers" value="${categorie.label}">
							</div>
						</div>
						<c:if test="${not empty utilisateur }">
						<div class="form-group row">
							<label for="edit" class="col-sm-2 col-form-label">Edit</label>
							<div class="col-sm-10">
								<a href="editCategorie?id=${categorie.id}" type="button" class="btn btn-primary"><i
									class="fas fa-edit"></i></a>
							</div>
						</div>
						</c:if>
					</form>

					<a class="btn btn-secondary" href="listCategorie?page=${page}&trier=${trier}" role="button">retour
						vers la liste</a>
				</div>
			</div>
		</div>
	</c:if>

	<%@include file="footer.jsp"%>
</body>
</html>